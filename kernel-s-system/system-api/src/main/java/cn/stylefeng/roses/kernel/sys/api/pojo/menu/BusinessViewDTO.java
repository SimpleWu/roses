package cn.stylefeng.roses.kernel.sys.api.pojo.menu;

import lombok.Data;

/**
 * 视图DTO
 *
 * @author fengshuonan
 * @since 2024/5/13 16:51
 */
@Data
public class BusinessViewDTO {

    /**
     * 视图id
     */
    private Long viewId;

    /**
     * 视图名称
     */
    private String viewName;

}
