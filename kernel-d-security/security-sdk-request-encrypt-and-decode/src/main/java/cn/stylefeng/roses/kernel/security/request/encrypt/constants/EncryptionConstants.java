package cn.stylefeng.roses.kernel.security.request.encrypt.constants;

/**
 * 请求解密，响应加密 常量
 *
 * @author luojie
 * @since 2021/3/23 12:54
 */
public interface EncryptionConstants {

    /**
     * 模块名
     */
    String ENCRYPTION_MODULE_NAME = "kernel-d-encryption";

    /**
     * 异常枚举的步进值
     */
    String ENCRYPTION_EXCEPTION_STEP_CODE = "29";

}
